package Cadastro;

public class Medico extends Usuario{

	private int numeroMedico;
	private String especialidadeMedico;
	
	public int getNumeroMedico() {
		return numeroMedico;
	}
	
	public void setNumeroMedico(int numeroMedico) {
		this.numeroMedico = numeroMedico;
	}
	
	public String getEspecialidadeMedico() {
		return especialidadeMedico;
	}
	
	public void setEspecialidadeMedico(String especialidadeMedico) {
		this.especialidadeMedico = especialidadeMedico;
	}
}
