package Cadastro;

public class Paciente extends Usuario {
	
	private String doenca;
	private String restricaoRemedio;
	private String nomeRemedio;
	
	public String getDoenca() {
		return doenca;
	}
	
	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}
	
	public String getRestricaoRemedio() {
		return restricaoRemedio;
	}
	
	public void setRestricaoRemedio(String restricaoRemedio) {
		this.restricaoRemedio = restricaoRemedio;
	}
	
	public String getNomeRemedio() {
		return nomeRemedio;
	}
	
	public void setNomeRemedio(String nomeRemedio) {
		this.nomeRemedio = nomeRemedio;
	}

}
