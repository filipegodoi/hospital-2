package Controle;
import java.util.ArrayList;

import Cadastro.Consulta;

public class ControleConsulta {

public ArrayList<Consulta> listaConsulta;
	
	public void ControlaConsulta(){
		listaConsulta = new ArrayList<Consulta>();
	}
	
	public void adicionar(Consulta consulta){
		listaConsulta.add(consulta);
	}
	
	public void remover(Consulta consulta){
		listaConsulta.remove(consulta);
	}
	
	public Consulta pesquisarConsulta(String tipoConsulta){
		for(Consulta consulta : listaConsulta){
			if(consulta.getNomeConsulta().equalsIgnoreCase(tipoConsulta)){
				return consulta;
			}
		}
		return null;
	}
	
	public void ConsultasNaLista(){
		for(Consulta consulta : listaConsulta){
			System.out.println(consulta.getnumeroRetorno());
		}
	}
	
	
}
